qmake
make
mkdir windows\data
copy client\release\openrecipes.exe windows\data
copy libsodium-win64\bin\libsodium-23.dll windows\data
cd windows\data
windeployqt --release --qmldir ..\..\client\qml openrecipes.exe
cd ..

if not exist data\libeay32.dll (
	echo Please copy libeasy32.dll into data\
	exit /b
)
if not exist data\ssleay32.dll (
	echo Please copy ssleay32.dll into data\
	exit /b
)

Paraffin.exe -update paraffin.wxs
move paraffin.PARAFFIN paraffin.wxs
candle.exe paraffin.wxs
candle.exe client.wxs
light.exe -out openrecipes-0.2.2.msi -ext WixUIExtension client.wixobj paraffin.wixobj
echo "DON'T FORGET TO UPDATE PRODUCT AND PACKAGE guidS BEFOR RELEASEING!!!"
