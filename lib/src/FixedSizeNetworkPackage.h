/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FIXED_SIZE_NETWORK_PACKAGE_H
#define FIXED_SIZE_NETWORK_PACKAGE_H

#include "NetworkPackage.h"

/*!
 * \brief Class to read a network package with a fixed size.
 */
class FixedSizeNetworkPackage : public NetworkPackage {
	private:
		/*!
		 * \brief Total size of the package in bytes.
		 */
		const quint64 size;

		quint64 bytesNeeded() const final;
	
	public:
		/*!
		 * \brief Constructor.
		 * \param size total size of the package in bytes
		 */
		explicit FixedSizeNetworkPackage(quint64 size);
};

#endif //FIXED_SIZE_NETWORK_PACKAGE_H
