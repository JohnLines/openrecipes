package org.jschwab.openrecipes;

import org.jschwab.openrecipes.Helpers;
import android.util.Log;
import android.os.Bundle;
import android.content.Intent;
import org.qtproject.qt5.android.bindings.QtActivity;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.widget.Toast;

public class OpenRecipesActivity extends QtActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handleIntent(getIntent());
	}
		
	@Override
	protected void onNewIntent(Intent i) {
		handleIntent(i);
		super.onNewIntent(i);
	}

	private void handleIntent(Intent i) {
		if (i.getAction() == Intent.ACTION_VIEW) {
			Log.d("OpenRecipes", "Got Intent.ACTION_VIEW");
			boolean succ;
			try {
				String recipeXml = Helpers.readFile(i.getData(), this);
				succ = Helpers.importRecipe(recipeXml);
			} catch (FileNotFoundException e) {
				succ = false;
			} catch (IOException e) {
				succ = false;
			}
			if (!succ) {
				Toast.makeText(this, "Can't import recipe", 5).show();
				//TODO translate
			}
		}
	}
}
