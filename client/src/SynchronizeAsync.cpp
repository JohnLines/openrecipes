/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SynchronizeAsync.h"
#include "SqlBackend.h"
#include "SecureClientConnection.h"
#include "EncryptedItem.h"
#include "Globals.h"

#include <InternalError.h>
#include <FixedSizeNetworkPackage.h>

#include <cstring>
#include <cassert>
#include <QTcpSocket>
#include <QtEndian>
#include <QEventLoop>

SynchronizeAsync::SynchronizeAsync(QObject *parent)
:
	QThread(parent),
	stop(false),
	connection(nullptr)
{}

SynchronizeAsync::~SynchronizeAsync() {
	mutex.lock();
	stop = true;
	mutex.unlock();
	if (connection) connection->deleteLater();
	connection = nullptr;
}

void SynchronizeAsync::checkStop() {
	bool tmp;
	mutex.lock();
	tmp = stop;
	mutex.unlock();
	if (tmp) throw IERROR("STOP");
}

void SynchronizeAsync::run() {
	/*
	connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error), [this](QTcpSocket::SocketError socketError __attribute__((unused))) {
			qDebug() << "Socket encountered an error";
			mutex.lock();
			stop = true;
			mutex.unlock();
			}
	);
	*/
	bool succ = true;
	try {
		SqlTransaction trans;
		SqlBackend::connectSignals(&trans);
		createConnection();
		checkStop();
		checkProtocolVersion();
		checkStop();
		const quint64 serverStartupTime = requestStartupTime();
		checkStop();
		if (serverStartupTime >= SqlBackend::getLastSyncTimestamp()) {
			SqlBackend::resetChanged(true);
		}
		checkStop();
		const std::vector<QByteArray> remoteIds = requestIds();
		checkStop();
		SqlBackend::deleteOthersIfNotChanged(remoteIds);
		checkStop();
		const std::vector<EncryptedItem> changedItemsRemote = requestChanged();
		checkStop();
		mitigateCollisions(changedItemsRemote);
		checkStop();
		writeToDb(changedItemsRemote);
		checkStop();
		sendChanged();
		checkStop();
		deleteLocalDeleted(remoteIds);
		checkStop();
		SqlBackend::resetChanged(false);
		checkStop();
		endSync();
		trans.commit();
	} catch (const InternalError &e) {
		succ = false;
	}
	emit done(succ);
	if (connection) connection->deleteLater();
	connection = nullptr;
}

void SynchronizeAsync::createConnection() {
	assert(connection == nullptr);
	connection = new SecureClientConnection(true);
	connect(connection, &SecureClientConnection::disconnected, [this]() {
			qDebug() << "Connection was unexpectedly closed";
			mutex.lock();
			stop = true;
			mutex.unlock();
			}
	);
	QEventLoop loop;
	connect(connection, &SecureClientConnection::cryptoEstablished, &loop, &QEventLoop::quit);
	connect(connection, &SecureClientConnection::disconnected, &loop, &QEventLoop::quit);
	loop.exec();
	//TODO check stop
}

void SynchronizeAsync::checkProtocolVersion() {
	qDebug() << "Requesting protocol version";
	connection->write(QByteArray("GETPV"));
	FixedSizeNetworkPackage numberNP(8);
	numberNP.readBlocking(connection);
	if (qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(numberNP.getData().constData())) != 1u) throw IERROR("Unsupported protocol version");
}

quint64 SynchronizeAsync::requestStartupTime() {
	qDebug() << "Requesting server startup time";
	connection->write(QByteArray("GETST"));
	FixedSizeNetworkPackage numberNP(8);
	numberNP.readBlocking(connection);
	return qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(numberNP.getData().constData()));
}

std::vector<QByteArray> SynchronizeAsync::requestIds() {
	qDebug() << "Requesting remote ids";
	connection->write(QByteArray("GETID"));
	FixedSizeNetworkPackage numberNP(8);
	numberNP.readBlocking(connection);
	const quint64 number = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(numberNP.getData().constData()));
	std::vector<QByteArray> ids;
	ids.reserve(number);
	for (quint64 i = 0; i < number; ++i) {
		FixedSizeNetworkPackage tmp(16);
		tmp.readBlocking(connection);
		ids.push_back(tmp.getData());
	}
	qDebug() << "Got " << ids.size() << " ids";
	return ids;
}	

std::vector<EncryptedItem> SynchronizeAsync::requestChanged() {
	qDebug() << "Requesting changed data";
	connection->write(QByteArray("GETCH"));
	QByteArray tmp;
	tmp.resize(16);
	*reinterpret_cast<quint64*>(tmp.data()) = qToBigEndian<quint64>(8);
	*reinterpret_cast<quint64*>(tmp.data() + 8) = qToBigEndian<quint64>(SqlBackend::getLastSyncTimestamp());
	connection->write(tmp);
	FixedSizeNetworkPackage countNP(8);
	countNP.readBlocking(connection);
	const quint64 count = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(countNP.getData().constData()));
	std::vector<EncryptedItem> data;
	data.reserve(count);
	for (quint64 i = 0; i < count; ++i) {
		FixedSizeNetworkPackage idNP(16);
		idNP.readBlocking(connection);
		FixedSizeNetworkPackage cipherSizeNP(8);
		cipherSizeNP.readBlocking(connection);
		FixedSizeNetworkPackage nonceSizeNP(8);
		nonceSizeNP.readBlocking(connection);
		const quint64 cipherSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(cipherSizeNP.getData().constData()));
		const quint64 nonceSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(nonceSizeNP.getData().constData()));
		FixedSizeNetworkPackage cipherNP(cipherSize);
		cipherNP.readBlocking(connection);
		FixedSizeNetworkPackage nonceNP(nonceSize);
		nonceNP.readBlocking(connection);
		data.emplace_back(idNP.getData(), cipherNP.getData(), nonceNP.getData());
	}
	qDebug() << "Got " << data.size() << " changed items from server";
	return data;
}

void SynchronizeAsync::mitigateCollisions(const std::vector<EncryptedItem> &changedItemsRemote) const {
	qDebug() << "Mitigating collisions";
	const std::forward_list<std::pair<QByteArray, SqlBackend::Table>> changedItemsLocal = SqlBackend::getChangedIds();
	for (const auto &lItem : changedItemsLocal) {
		for (const auto &rItem : changedItemsRemote) {
			if (lItem.first == rItem.getId()) {
				if (lItem.second == SqlBackend::Table::Recipes) {
					const EncryptedRecipe er(rItem);
					if (er.getData() == lItem.first) {
						qDebug() << "Recipe data not changed, don't need to mitigate something";
						break;
					}
					qDebug() << "Changing name";
					const QString name = SqlBackend::getNameForRecipe(lItem.first);
					SqlBackend::setNameForRecipe(lItem.first, name + tr(" (local version)"));
				}
				qDebug() << "Changing id";
				SqlBackend::changeId(lItem.first, lItem.second);
				break;
			}
		}
	}
}

void SynchronizeAsync::sendChanged() {
	qDebug() << "Sending changed items to server";
	const std::forward_list<EncryptedItem> changedItems = SqlBackend::getChangedEnc();
	if (changedItems.empty()) return;
	
	for (const auto &i : changedItems) {
		connection->write(QByteArray("ADDUP"));
		QByteArray tmp;
		tmp.resize(16);
		const quint64 cipherSizeBE = qToBigEndian<quint64>(i.getCipher().size());
		const quint64 nonceSizeBE = qToBigEndian<quint64>(i.getNonce().size());
		std::memcpy(tmp.data(), &cipherSizeBE, 8);
		std::memcpy(tmp.data() + 8, &nonceSizeBE, 8);
		QByteArray size;
		size.resize(8);
		*reinterpret_cast<quint64*>(size.data()) = qToBigEndian<quint64>(16 + 2 * 8 + i.getCipher().size() + i.getNonce().size());
		connection->write(size);
		connection->write(i.getId());
		connection->write(tmp);
		connection->write(i.getCipher());
		connection->write(i.getNonce());
		FixedSizeNetworkPackage res(2);
		res.readBlocking(connection);
		if (res.getData() != QByteArray("OK")) throw IERROR("Server refused ADDUP");
	}
}

void SynchronizeAsync::deleteLocalDeleted(const std::vector<QByteArray> &remoteIds) {
	qDebug() << "deleteLocalDeleted()";
	const std::forward_list<QByteArray> localIds = SqlBackend::getAllIds();
	QByteArray idSize;
	idSize.resize(8);
	*reinterpret_cast<quint64*>(idSize.data()) = qToBigEndian<quint64>(16);
	for (const auto &rId : remoteIds) {
		bool deleted = true;
		for (const auto &lId : localIds) {
			if (rId == lId) {
				deleted = false;
				break;
			}
		}
		if (deleted) {
			qDebug() << "Deleting one item from server";
			connection->write("DELET");
			connection->write(idSize);
			connection->write(rId);
			FixedSizeNetworkPackage res(2);
			res.readBlocking(connection);
			if (res.getData() != QByteArray("OK")) throw IERROR("Server can't delete items");
		}
	}
}

void SynchronizeAsync::endSync() {
	qDebug() << "Closing connection";
	connection->write("CLOSE");
	FixedSizeNetworkPackage timestampNP(8);
	timestampNP.readBlocking(connection);
	const quint64 timestamp = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(timestampNP.getData().constData()));
	qDebug() << "Got timestamp " << timestamp;
	SqlBackend::updateLastSyncTimestamp(timestamp);
}

void SynchronizeAsync::writeToDb(const std::vector<EncryptedItem> &changedItemsRemote) const {
	qDebug() << "Writing " << changedItemsRemote.size() << " changed items to db";
	for (const auto &i : changedItemsRemote) SqlBackend::toDB(i);
}

void SynchronizeAsync::cancel() {
	mutex.lock();
	stop = true;
	mutex.unlock();
}
