/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Controls 2.3

SpinBox {
	property real realValue

	from: 0
	to: 999999 * 100
	stepSize: 100
	editable: true

	validator: DoubleValidator {
		bottom: 0
		top: 100 * 100
	}

	textFromValue: function(value, locale) {
		return Number(value/100).toLocaleString(locale);
	}

	valueFromText: function(text, locale) {
		return Number.fromLocaleString(locale, text) * 100;
	}

	onRealValueChanged: value = realValue * 100
	onValueChanged: realValue = value / 100
}
	
