/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ASYNC
#define ASYNC

#include "HelperDefs.h"

#include <QObject>
#include <thread>

class TcpConnection;
struct DbItem;

class Async : public QObject {
	Q_OBJECT

	private slots:
		void onGotIds(const GetIdsRes &res);
		void onAddedOrUpdated(bool succ);
		void onDeleted(bool succ);
		void onSettedSyncKey(bool succ);
		void onGotSyncKey(const GetSyncKeyRes &res);
		void onGotChanged(const GetChangedRes &res);

	public:
		static void getIds(TcpConnection *con, quint64 userId);
		static void addOrUpdate(TcpConnection *con, const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce, quint64 userId);
		static void deleteEntry(TcpConnection *con, const QByteArray &id, quint64 userId);
		static void getChanged(TcpConnection *con, quint64 timestamp, quint64 userId);
		static void setSyncKey(TcpConnection *con, const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce);
		static void getSyncKey(TcpConnection *con, const QByteArray &id);

	signals:
		void gotIds(const GetIdsRes &res);
		void addedOrUpdated(bool succ);
		void deleted(bool succ);
		void settedSyncKey(bool succ);
		void gotSyncKey(const GetSyncKeyRes &res);
		void gotChanged(const GetChangedRes &res);
};

#endif //ASYNC
