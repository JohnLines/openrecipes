/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "BackendWorker.h"
#include "DbItem.h"
#include "SqlBackend.h"
#include "TcpServer.h"
#include "UnixSignalHandler.h"
#include "Config.h"

#include <InternalError.h>
#include <config.h>

#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QMetaType>
#include <QSettings>
#include <QTimer>
#include <argp.h>
#include <csignal>
#include <iostream>
#include <memory>
#include <sodium.h>
#include <sys/stat.h>
#include <syslog.h>
#include <unistd.h>
#include <vector>

Q_DECLARE_METATYPE(GetIdsRes);
Q_DECLARE_METATYPE(GetChangedRes);

static void printVersion(FILE *stream, struct argp_state*) {
	fprintf(stream, "OpenRecipesServer%s %s\n", config::namepostfix, config::version);
}

void (*argp_program_version_hook) (FILE*, struct argp_state*) = printVersion;

const char *argp_program_bug_address = "<openrecipes@jschwab.org>";

static error_t parseCmdLine(int key, char *arg, struct argp_state *state) {
	Config *opts = reinterpret_cast<Config*>(state->input);
	switch (key) {
		case 'f':
			opts->runInForeground = true;
			break;
		case 'c':
			opts->configFile = arg;
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static void daemonize() {
	pid_t pid = fork();
	if (pid < 0) {
		std::cerr << "Can't fork\n";
		exit(EXIT_FAILURE);
	}
	if (pid != 0) exit(EXIT_SUCCESS);
	if (setsid() < 0) exit(EXIT_FAILURE);
	signal(SIGHUP, SIG_IGN);
	pid = fork();
	if (pid < 0) exit(EXIT_FAILURE);
	if (pid != 0) exit(EXIT_SUCCESS);
	if (chdir("/") != 0) exit(EXIT_FAILURE);
	umask(0);
	for (size_t i = sysconf(_SC_OPEN_MAX); i > 0; --i) close(i);
}

static void syslogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
	const QByteArray localMsg = msg.toLocal8Bit();
	int pri;
	switch (type) {
		case QtDebugMsg:
			pri = LOG_DEBUG;
			break;
		case QtInfoMsg:
			pri = LOG_INFO;
			break;
		case QtWarningMsg:
			pri = LOG_WARNING;
			break;
		case QtCriticalMsg:
			pri = LOG_CRIT;
			break;
		case QtFatalMsg:
			pri = LOG_ERR;
			break;
	}
	syslog(pri, "%s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
}

static Config getConfig(int argc, char *argv[]) {
	Config conf;
	struct argp_option options[] = {
		{"foreground", 'f', nullptr, 0, "Don't daemonize", 0},
		{"config", 'c', "FILE", 0, "Config file (default: /etc/openrecipesserver)", 0},
		{nullptr, 0, nullptr, 0, nullptr, 0}
	};
	struct argp argp = {options, parseCmdLine, nullptr, nullptr, nullptr, nullptr, nullptr};
	conf.runInForeground = false;
	conf.configFile = "/etc/openrecipesserver";
	argp_parse(&argp, argc, argv, 0, 0, &conf);

	qDebug() << "Using config file" << conf.configFile;
	if (access(conf.configFile.toStdString().c_str(), F_OK) == -1) {
		std::cerr << "Can't access config file " << conf.configFile.toStdString() << "\n";
		exit(EXIT_FAILURE);
	}

	QSettings settings(conf.configFile, QSettings::IniFormat);
	if (!settings.contains("mysql/database")) {
		std::cerr << "No mysql/database in conf file\n";
		exit(EXIT_FAILURE);
	}
	if (!settings.contains("mysql/user")) {
		std::cerr << "No mysql/user in conf file\n";
		exit(EXIT_FAILURE);
	}
	if (!settings.contains("mysql/password")) {
		std::cerr << "No mysql/password in conf file\n";
		exit(EXIT_FAILURE);
	}
	if (!settings.contains("server/port")) {
		std::cerr << "No server/port in conf file\n";
		exit(EXIT_FAILURE);
	}
	if (!settings.contains("jobs")) {
		std::cerr << "No general/jobs in conf file\n";
		exit(EXIT_FAILURE);
	}
	if (!settings.contains("mysql/host")) qDebug() << "No mysql/host in conf file, using localhost";
	if (!settings.contains("mysql/port")) qDebug() << "No mysql/port in conf file, using 3306";
	conf.sql.database = settings.value("mysql/database").toString();
	conf.sql.user = settings.value("mysql/user").toString();
	conf.sql.password = settings.value("mysql/password").toString();
	conf.sql.host = settings.value("mysql/host", "localhost").toString();
	conf.sql.port = settings.value("mysql/port", 3306).toInt();
	conf.server.port = settings.value("server/port").toInt();
	conf.jobs = settings.value("jobs").toInt();
	if (conf.jobs < 1) {
		std::cerr << "Invalid number for general/jobs. Must be >= 1\n";
		exit(EXIT_FAILURE);
	}

	return conf;
}

int main(int argc, char *argv[]) {
	Config conf = getConfig(argc, argv);

	qInfo() << "Starting OpenRecipesServer with config file" << conf.configFile;
	if (conf.runInForeground) {
		qInfo() << "Staying in foreground";
	} else {
		qInfo() << "Daemonizing...";
	}

	if (!conf.runInForeground) {
		qDebug() << "Daemonize";
		daemonize();
		openlog("OpenRecipesServer", LOG_NDELAY, LOG_DAEMON);
		qInstallMessageHandler(syslogMessageHandler);
	} else {
		qDebug() << "Don't daemonize";
	}

	QCoreApplication app(argc, argv);
	app.setApplicationName(QString("OpenRecipesServer%1").arg(config::namepostfix));
	app.setApplicationVersion(config::version);

	qRegisterMetaType<GetIdsRes>("GetIdsRes");
	qRegisterMetaType<GetChangedRes>("GetChangedRes");
	qRegisterMetaType<GetSyncKeyRes>("GetSyncKeyRes");

	if (sodium_init() == -1) {
		qCritical() << "Can't init libsodium!";
		return EXIT_FAILURE;
	}

	try {
		SqlBackend::init(conf.sql);
	} catch (const InternalError &e) {
		qCritical() << "Can't init sql database: " << e.what();
		return EXIT_FAILURE;
	}

	qDebug() << "Starting" << conf.jobs << "Background worker";
	std::vector<std::unique_ptr<BackendWorker>> bw;
	try {
		for (unsigned int i = 0; i < conf.jobs; ++i) bw.emplace_back(new BackendWorker());
	} catch (const InternalError &e) {
		qCritical() << "Can't start backend worker";
		exit(EXIT_FAILURE);
	}
	for (auto &b : bw) b->start();

	UnixSignalHandler sigHandler; //TODO this may throw
	TcpServer server(conf.server);
	QObject::connect(&sigHandler, &UnixSignalHandler::closeGently, &server, &TcpServer::onCloseGently);
	QObject::connect(&sigHandler, &UnixSignalHandler::printInfo, &server, &TcpServer::onPrintInfo);
	for (auto &w : bw) {
		QObject::connect(&server, &TcpServer::closed, w.get(), &BackendWorker::onClose);
		QObject::connect(
				w.get(),
				&QThread::finished,
				&app,
				[&app, &bw]() {
					for (const auto &t : bw) {
						if (t->isRunning()) return;
					}
					qInfo() << "Stopped";
					QTimer::singleShot(0, &app, &QCoreApplication::quit);
				}
		);
	}

	qDebug() << "Starting event loop...";
	return app.exec();
}
