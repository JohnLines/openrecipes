/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "RequestParser.h"
#include "InternalError.h"
#include "Request.h"
#include "SecureServerConnection.h"

#include <FixedSizeNetworkPackage.h>

RequestParser::RequestParser(QObject *parent, SecureServerConnection *connection)
:
	QObject(parent),
	connection(connection),
	currentNP(dynamic_cast<NetworkPackage*>(new FixedSizeNetworkPackage(Request::keywordSize))),
	state(State::ReadingKeyword)
{
	connect(currentNP.get(), &NetworkPackage::complete, this, &RequestParser::onNPComplete);
	currentNP->read(connection);
}

void RequestParser::onNPComplete() {
	switch(state) {
		case State::ReadingKeyword:
			qDebug() << "Got keyword " << currentNP->getData();
			{
				std::shared_ptr<Request> r;
				try {
					r = Request::getRequest(currentNP->getData());
				} catch (const InternalError &e) {
					emit error();
					return;
				}
				if (r->completed()) {
					emit newRequest(r);
					currentNP.reset(dynamic_cast<NetworkPackage*>(new FixedSizeNetworkPackage(Request::keywordSize)));
				} else {
					currentNP = std::dynamic_pointer_cast<NetworkPackage>(r);
					state = State::ReadingRequest;
				}
			}
			break;
		case State::ReadingRequest:
			qDebug() << "Request completed";
			emit newRequest(std::dynamic_pointer_cast<Request>(currentNP));
			currentNP.reset(dynamic_cast<NetworkPackage*>(new FixedSizeNetworkPackage(Request::keywordSize)));
			state = State::ReadingKeyword;
			break;
	}
			
	connect(currentNP.get(), &NetworkPackage::complete, this, &RequestParser::onNPComplete);
	currentNP->read(connection);
}
